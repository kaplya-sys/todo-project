const { Pool } = require('pg')
const config = require('../config/dbConfig')

const pool = new Pool({
    user: config.USER,
    host: config.HOST,
    database: config.DB,
    password: config.PASSWORD,
    port: config.PORT
})

const insertValues = async(client) => {
    const result = await client.query('SELECT * FROM todos')
    if (!result.rows[0]) {
        await client.query(`INSERT INTO todos (todolist) VALUES ('[]')`)
    }
}

;(async () => {
    const client = await pool.connect()

    console.log('\x1b[32m%s\x1b[0m','Connection has been established successfully.')

    try {
        await client.query('CREATE TABLE IF NOT EXISTS todos (todolist JSON)')
        await insertValues(client)
    } finally {
        client.release()
    }

})().catch(err => console.log('\x1b[31m%s\x1b[0m', err.stack))

module.exports = {
    query: (text, params) => pool.query(text, params)
  }