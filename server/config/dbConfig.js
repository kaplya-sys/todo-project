require('dotenv').config()

const {DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, DB_PORT} = process.env

module.exports = {
    HOST: DB_HOST || "postgres_db",
    USER: DB_USER || "worker",
    PASSWORD: DB_PASSWORD  || "worker",
    DB: DB_NAME || "tododb",
    PORT: DB_PORT || 5432
}