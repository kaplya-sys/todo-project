const express = require('express')
const router = express.Router()
const db = require('../db/index')

router.get('/todo', async(req, res) => {
    try {
        const result = await db.query('SELECT * FROM todos')
        res.status(200).json(result.rows[0])
        console.log('\x1b[32m%s\x1b[0m', 'Data received successfully')
    } catch(err) {
        console.log('\x1b[31m%s\x1b[0m','Error receiving data: ', err)
    }
})

router.patch('/todo', async(req, res) => {
    const values = JSON.stringify(req.body)
      try {
        await db.query('UPDATE todos SET todolist = ($1)', [values])
        res.status(200).send({ message: 'ok' })
        console.log('\x1b[32m%s\x1b[0m', 'Data has been successfully updated')
      } catch(err) {
        console.log('\x1b[31m%s\x1b[0m','Error updating data: ', err)
      }
})

module.exports = router