const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const todoRoute = require('./routes/todo.route')
const config = require('./config/dbConfig')
require('dotenv').config()

const PORT = process.env.PORT || 5000
const corsOptions = {
    origin: 'http://localhost:3000'
}

const app = express()

app.use(cors(corsOptions))

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))

app.use('/api', todoRoute)

app.listen(PORT, () => console.log('\x1b[34m%s\x1b[0m',`Server started... on port: ${PORT}`))
