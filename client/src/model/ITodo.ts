export interface ITodo {
    id: number
    todo: string
    isChecked: boolean
    setColor: string
    setDecoration: string
    value: string
    defaultValue: string
}