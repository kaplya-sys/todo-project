type valueTypes = Record<string, string>

const valueVariables: valueTypes = {
    all: 'all',
    done: 'done',
    active: 'active',
    alt: 'Page not found'
}

const styleVariables: valueTypes = {
    none: 'none',
    lineThrough: 'line-through',
    darkFont: 'black',
    lightFont: 'white',
    lightWindow: 'light-window',
    darkWindow: 'dark-window',
    lightTheme: 'light',
    darkTheme: 'dark',
    lightInput: 'light-block',
    darkInput: 'dark-block',
    width: "795",
    height: "340"
}

const classNameVariables: valueTypes = {
    image: 'image',
    back: 'back',
    blockBtn: 'blockBtn',
    btn: 'btn',
    btnColor: 'btnColor',
    blockInput: 'blockInput',
    input: 'input',
    btnInput: 'btnInput',
    mainBox: 'main-box',
    todoLink: 'todoLink',
    container: 'container',
    mainLink: 'mainLink',
    todoStyle: 'todoList',
    checkbox: 'checkbox',
    window: 'window',
    btnToggle: 'btn-toggle',
    loader: 'loader',
    headingMain: 'heading',
    paragraphMain: 'paragraph',
    errorStyle: 'error'
    
}

const routeVariables: valueTypes = {
    MainRoute: '/',
    TodoRoute: 'todo',
    NotFoundRoute: '*'
}


export {valueVariables, styleVariables, classNameVariables, routeVariables}