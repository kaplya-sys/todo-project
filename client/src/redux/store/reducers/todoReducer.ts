import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {valueVariables, styleVariables} from "../../../config/variables"
import {IState} from "./IState"
import {ITodo} from "../../../model/ITodo"
import {getTodoAsync} from "../../asyncThunk/getTodoAsync"

const{all, done, active} = valueVariables
const {lineThrough, none} = styleVariables

const initialState: IState = {
    todoList: [],
    todoText: '',
    value: all,
    color: '',
    isLoading: false,
    error: null
}

export const todoReducer = createSlice({
    name: 'todoSlice',
    initialState,
    reducers:{
        addTodo: state => {
            const {todoText, todoList} = state
            const text = todoText.trim()

            if (text) {
                const todo = todoList.find(item => item.todo === text)

                    if (todo) {
                        alert('Данная задача уже в работе')
                    } else {
                        const newTodo: ITodo = {
                            id: new Date().getTime(),
                            todo: text,
                            isChecked: false,
                            setColor: none,
                            setDecoration: none,
                            value: active,
                            defaultValue: all
                        }
                        state.todoList.push(newTodo)
                        state.todoText = ''
                    }
                } else {
                    alert('Пустая строка')
            }
        },
        addTodoText: (state, action: PayloadAction<string>) => {
            state.todoText = action.payload
        },
        selectChecked: (state, action: PayloadAction<number>) => {
            state.todoList = state.todoList.map(item => 
                item.id === action.payload ?
                    {...item, isChecked: !item.isChecked} :
                item)
        },
        setHighlightTodo: (state, action: PayloadAction<string>) => {
            state.todoList = state.todoList.map(item =>
                item.isChecked ?
                { ...item, setColor: action.payload, isChecked: !item.isChecked} :
                item)
            
                state.color = action.payload
        },
        onDoneTodo: state => {
            state.todoList = state.todoList.map(item => 
                item.isChecked ?
                { ...item, value: done, setDecoration: lineThrough, isChecked: !item.isChecked} :
                item)
        },
        removeAllTodo: state => {
            state.todoList = []
        },
        selectValue: (state, action: PayloadAction<string>) => {
            state.value = action.payload
        }
    },
    extraReducers: builder => {
        builder
            .addCase(getTodoAsync.pending, state => {
                state.isLoading = true
                state.error = null
            })
            .addCase(getTodoAsync.fulfilled, (state, action: PayloadAction<any>) => {
                state.isLoading = false
                state.todoList = action.payload
        })
            .addCase(getTodoAsync.rejected, (state, action: PayloadAction<string>) => {
                state.error = action.payload
                state.isLoading = false
        }) 
    }
})

export const {
  addTodo,
  addTodoText,
  selectChecked,
  setHighlightTodo,
  onDoneTodo,
  removeAllTodo,
  selectValue
} = todoReducer.actions
