import {ITodo} from "../../../model/ITodo"

export interface IState {
    todoList: ITodo[]
    todoText: string
    value: string
    color: string
    isLoading: boolean
    error: string | null
}