import {createAsyncThunk} from '@reduxjs/toolkit'
import {ITodo} from "../../model/ITodo"

export const setTodoAsync = createAsyncThunk(
    'todoSlice/setTodo', async(todoList: ITodo[]) => {
        if (todoList.length) {
            try {
                await fetch('/api/todo', {
                    method: 'PATCH',
                    headers: {
                      'Content-Type': 'application/json;charset=utf-8'
                    },
                    body: JSON.stringify(todoList)
                  })
            } catch(err) {
                console.log(err)
            }
        }
        
    }
)