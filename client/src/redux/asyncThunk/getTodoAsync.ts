import {createAsyncThunk} from '@reduxjs/toolkit'
import {ITodo} from "../../model/ITodo"


export const getTodoAsync = createAsyncThunk<ITodo, void, {rejectValue: string}>(
    'todoSlice/getTodo', async(_, {rejectWithValue}) => {
        try {
            const res = await fetch('/api/todo')
            if (!res.ok) {
                throw new Error('server error!')
            }
            const json = await res.json()
            return json.todolist as ITodo
        } catch(err) {
            return rejectWithValue(err.message)
        }
    }
)
