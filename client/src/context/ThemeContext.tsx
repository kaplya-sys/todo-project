import {createContext} from "react"
import {IContextType} from "./type"

export const ThemeContext = createContext<IContextType>({
    isLight: true
})