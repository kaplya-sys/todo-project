import React from "react"
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import {Main} from "./components/main/Main"
import {NotFound} from "./components/notFound/NotFound"
import {Todo} from "./components/todo/Todo"
import {routeVariables} from "./config/variables"

const App = () => {
    const {MainRoute, TodoRoute, NotFoundRoute} = routeVariables

    return (
        <BrowserRouter>
            <Routes>
                <Route path={MainRoute} element={<Main/>}/>
                <Route path={TodoRoute} element={<Todo/>}/>
                <Route path={NotFoundRoute} element={<NotFound/>}/>
            </Routes>
        </BrowserRouter>
    )
}

export default App