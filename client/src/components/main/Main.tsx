import React, {FC} from "react"
import {Link} from "react-router-dom"
import { classNameVariables, routeVariables } from "../../config/variables"
import "./main.css"

export const Main: FC = () => {
    const {mainBox, todoLink, headingMain, paragraphMain} = classNameVariables
    const {TodoRoute} = routeVariables

    return (
        <div className={mainBox}>
            <h2 className={headingMain}>Главная</h2>

            <p className={paragraphMain}>Темирбулатов Руслан Махмудович</p>

            <Link className={todoLink} to={TodoRoute}>К списку дел:</Link>
        </div>
    )
}