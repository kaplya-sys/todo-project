import React, {FC, ChangeEvent, MouseEvent} from "react"
import {useAppSelector, useAppDispatch} from '../../hooks/hooks'
import {
    onDoneTodo,
    removeAllTodo,
    selectValue,
    setHighlightTodo
} from "../../redux/store/reducers/todoReducer"
import {classNameVariables, valueVariables} from "../../config/variables"
import './buttons.css'

export const Buttons: FC = () => {
    const {all, active, done} = valueVariables
    const {blockBtn, btnColor, btn} = classNameVariables
    const {color, value} = useAppSelector(state => state.todoState)
    const dispatch = useAppDispatch()

    return (
        <div className={blockBtn}>
            <input
                className={`${btn} ${btnColor}`}
                type="color"
                value={color}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                    dispatch(setHighlightTodo(event.target.value))}
            />

            <button
                className={btn}
                onClick={(event: MouseEvent<HTMLButtonElement>) =>
                    dispatch(onDoneTodo())}
            >
                Выполнено
            </button>

            <button
                className={btn}
                onClick={(event: MouseEvent<HTMLButtonElement>) =>
                    dispatch(removeAllTodo())}
            >
                Очистить
            </button>

            <select
                className={btn}
                value={value}
                onChange={(event: ChangeEvent<HTMLSelectElement>) =>
                    dispatch(selectValue(event.target.value))}
            >
                <option value={all}>Все</option>
                <option value={done}>Выполненные</option>
                <option value={active}>Активные</option>
            </select>
        </div>
    )
}
