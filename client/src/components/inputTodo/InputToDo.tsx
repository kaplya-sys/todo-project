import React, {
    FC,
    KeyboardEvent,
    ChangeEvent,
    MouseEvent,
    useContext,
    useMemo
} from "react"
import {useAppSelector, useAppDispatch} from '../../hooks/hooks'
import {addTodoText, addTodo} from "../../redux/store/reducers/todoReducer"
import {ThemeContext} from "../../context/ThemeContext"
import {Buttons} from "../buttons/Buttons"
import {classNameVariables, styleVariables} from "../../config/variables"
import './inputToDo.css'

export const InputToDo: FC = () => {
    const {lightInput, darkInput} = styleVariables
    const {blockInput, input, btnInput} = classNameVariables
    const {isLight} = useContext(ThemeContext)
    const dispatch = useAppDispatch()
    const text = useAppSelector(state => state.todoState.todoText)

    const handleKeypress = (event: KeyboardEvent<HTMLInputElement>) =>
    {
        event.key === 'Enter' && dispatch(addTodo())
    }

    const changeThemeColor = useMemo((): string => {
        return isLight ? lightInput: darkInput
    }, [isLight])

    return (
        <div className={`${blockInput} ${changeThemeColor}`}>
            <input
                tabIndex={2}
                className={input}
                type="text"
                value={text}
                onChange={(event: ChangeEvent<HTMLInputElement>) =>
                    dispatch(addTodoText(event.target.value))}
                onKeyPress={handleKeypress}
            />

            <button
                tabIndex={3}
                className={btnInput}
                onClick={(event: MouseEvent<HTMLButtonElement>) => dispatch(addTodo())}
            >
                Добавить
            </button>

            <Buttons />
        </div>
    )
}