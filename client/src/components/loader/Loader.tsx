import React, {FC} from "react"
import {classNameVariables} from "../../config/variables"
import "./loader.css"

export const Loader: FC = () => {
    const {loader} = classNameVariables

    return <div className={loader}/>
}