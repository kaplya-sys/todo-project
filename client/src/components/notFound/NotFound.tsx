import React, {FC} from "react"
import {Link} from "react-router-dom"
import { styleVariables, valueVariables, classNameVariables, routeVariables } from "../../config/variables"
import "./notFound.css"

export const NotFound: FC = () => {
    const {width, height} = styleVariables
    const {MainRoute} = routeVariables
    const {alt} = valueVariables
    const {image, back} = classNameVariables

    return (
        <div>
            <img
                className={image}
                src={`${process.env.PUBLIC_URL}/images/404.webp`}
                alt={alt}
                width={width}
                height={height}
                />
            <Link className={back} to={MainRoute}>Вернитесь на главную страницу</Link>
        </div>
    )
}