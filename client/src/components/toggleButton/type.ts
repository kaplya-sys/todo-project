export interface IToggleButtonProps {
    handleChange(): void
}