import React, {FC} from "react"
import {classNameVariables} from "../../config/variables"
import {IToggleButtonProps} from "./type"
import './toggleButton.css'

export const ToggleButton: FC<IToggleButtonProps> = ({handleChange}) => {
    const {btnToggle} = classNameVariables

    return (
        <button tabIndex={1} onClick={handleChange} className={btnToggle}>
            Сменить тему
        </button>
    )
}