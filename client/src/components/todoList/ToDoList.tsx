import React, {useContext, useMemo, FC} from "react"
import {useAppSelector, useAppDispatch} from '../../hooks/hooks'
import {selectChecked} from "../../redux/store/reducers/todoReducer"
import {ThemeContext} from "../../context/ThemeContext"
import {ITodo} from "../../model/ITodo"
import {withLoader} from "../../hoc/WithLoader"
import {styleVariables, classNameVariables} from "../../config/variables"
import './todoList.css'

const ToDoList: FC = () => {
    const {darkFont, lightFont} = styleVariables
    const {todoStyle, checkbox, errorStyle} = classNameVariables
    const {todoList, value, error} = useAppSelector(state => state.todoState)
    const dispatch = useAppDispatch()
    const {isLight} = useContext(ThemeContext)

    const filterList = useMemo((): ITodo[] => todoList.filter(item =>
        item.value === value ||
        item.defaultValue === value
    ), [todoList, value])

    const changeThemeColor = useMemo((): string => {
        return isLight ? darkFont: lightFont
    }, [isLight])

    return (
        <>
            {error && <h2 className={errorStyle}>{error}</h2>}

            <ul className={todoStyle}>
                {filterList.map(item => {
                    const {
                        id,
                        todo,
                        isChecked,
                        setColor,
                        setDecoration
                    } = item
                    const styleTodo = {
                        background: setColor,
                        textDecoration: setDecoration,
                        color: changeThemeColor
                    }

                    return (
                        <li key={id} style={styleTodo}>
                            <input type="checkbox"
                                className={checkbox}
                                checked={isChecked}
                                onChange={() => dispatch(selectChecked(id))}
                            />

                            <span>{todo}</span>
                        </li>
                    )
                })}
            </ul>
        </>
    )
}

export default withLoader(ToDoList)