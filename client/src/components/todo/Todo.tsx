import React, {FC, useEffect, useState} from "react"
import {useMemo} from "react"
import {Link} from "react-router-dom"
import {useAppDispatch, useAppSelector} from '../../hooks/hooks'
import {getTodoAsync} from "../../redux/asyncThunk/getTodoAsync"
import {setTodoAsync} from "../../redux/asyncThunk/setTodoAsync"
import {ThemeContext} from "../../context/ThemeContext"
import {ToDoWindow} from "../todoWindow/ToDoWindow"
import {InputToDo} from "../inputTodo/InputToDo"
import {ToggleButton} from "../toggleButton/ToggleButton"
import {styleVariables, classNameVariables, routeVariables} from "../../config/variables"
import './todo.css'

export const Todo: FC = () => {
    const {lightTheme, darkTheme} = styleVariables
    const {container, mainLink} = classNameVariables
    const {MainRoute} = routeVariables
    const dispatch = useAppDispatch()
    const todoList = useAppSelector(state => state.todoState.todoList)
    const [isLight, setIsLight] = useState<boolean>(true)

    useEffect(():void => {
        dispatch(getTodoAsync())
    }, [])

    useEffect(():void => {
        dispatch(setTodoAsync(todoList))
    }, [todoList])

    const changeTheme = ():void => {
        setIsLight(!isLight)
    }

    const changeThemeColor = useMemo((): string => {
        return isLight ? lightTheme : darkTheme
    }, [isLight])

    return (
        <div className={`${container} ${changeThemeColor}`}>
            <ThemeContext.Provider value={{isLight}}>
                <ToDoWindow />
                <InputToDo />
            </ThemeContext.Provider>
            <ToggleButton handleChange={changeTheme}/>
            <Link className={mainLink} to={MainRoute}>На главную</Link>
        </div>
    )
}