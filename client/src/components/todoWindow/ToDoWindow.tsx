import React, {FC, useContext, useMemo} from "react"
import {useAppSelector} from "../../hooks/hooks"
import {ThemeContext} from "../../context/ThemeContext"
import ToDoList from "../todoList/ToDoList"
import {styleVariables, classNameVariables} from "../../config/variables"
import "./todoWindow.css"

export const ToDoWindow: FC = () => {
    const {lightWindow, darkWindow} = styleVariables
    const {window} = classNameVariables
    const {isLight} = useContext(ThemeContext)
    const isLoading = useAppSelector(state => state.todoState.isLoading)

    const changeThemeColor = useMemo((): string => {
        return isLight ? lightWindow: darkWindow
    }, [isLight])
        
    return (
        <div>
            <h1>Список дел</h1>
            <div className={`${window} ${changeThemeColor}`}>
                <ToDoList isLoading={isLoading}/>
            </div>
        </div>
    )
}