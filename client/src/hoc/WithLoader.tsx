import React, {FC, ComponentType} from "react"
import {Loader} from "../components/loader/Loader"
import {ILoaderProps} from "./type"

export const withLoader = <P extends object>(WrappedComponent:ComponentType<P>): FC<P & ILoaderProps> =>
    ({isLoading, ...props}:ILoaderProps) =>
    {
    return isLoading? <Loader />: <WrappedComponent {...props as P}/>
}